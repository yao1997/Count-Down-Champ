import React,{Component} from 'react';
import './App.css'
class Clock extends Component{
  constructor(props){
    super(props);
    this.state={
      second:'0',
      minute:'0',
      hour:'0',
      day:'0'
    }
  }

  componentDidMount(){
    setInterval(()=>{this.getTimeUntil(this.props.deadline),1000});

  }

  getTimeUntil(deadline){
    var difference=Date.parse(deadline)-Date.parse(new Date());
    var seconds=Math.floor((difference/1000) %60);
    var minutes=Math.floor((difference/(1000*60)) %60);
    var hours=Math.floor((difference/(1000*60*60)) %24);
    var days=Math.floor((difference/(1000*60*60*24)));
    console.log("days: ",days," minutes: ",minutes," hours ",hours, " seconds: ",seconds);
    this.setState({second:seconds,minute:minutes,hour:hours,day:days});
  }

  render(){
    return(
      <div>
        <div className='Clock-days'>{this.state.day} days</div>
        <div className='Clock-hours'>{this.state.hour} hour</div>
        <div className='Clock-minutes'>{this.state.minute} minute</div>
        <div className='Clock-seconds'>{this.state.second} second</div>
      </div>
    );
  }

}
export default Clock;
